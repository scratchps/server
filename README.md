# SCRATCH #

This is simply a test project to see how I can make my first framework.

### What is this repository for? ###

* Try out new things that seem useful but aren't seen in usual bases.
* Test knowledge.
* Learn new things!

### How do I get set up? ###

* This project uses Maven to handle dependencies. The project uses KryoNet, Jython, Guava, and FindBugs IDE libraries.

### What "new things" are you talking about? ###

* KryoNet instead of NIO, Netty, and other networking works. It seems of relative ease and through some work, I believe it will make networking a lot more logical and easy to use rather than being a scary behemoth.
* PLANNED: Ashley, a Java entity system.
* More to come hopefully!

### UPDATE ###
I (Arham 4) strongly recommend you not use this as a base for a project, rather as a reference to realize the structurization of networking. This project uses the outdated library "KryoNet," which is highly discouraged to use to due to its last update being in 2013.