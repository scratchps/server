package co.arham.scratch.utilities;

import co.arham.scratch.Configurations;
import com.esotericsoftware.minlog.Log;
import org.python.util.PythonInterpreter;

import java.io.File;
import java.util.HashMap;

/**
 * Manages the Python scripts for the server.
 *
 * @author Tyler Telis <tyler@xlitersps.com>
 * @author Arham 4
 * @version 1.1
 * @revised April 17, 2016
 * @since Jul 19, 2013
 */
public class PythonScriptManager {

    /**
     * The pythonCache map used for storing the python instances
     */
    private static final HashMap<String, String> pythonCache = new HashMap<>();

    /**
     * The {@code PythonInterpreter} singleton.
     */
    private static final PythonInterpreter pythonInterpreter = new PythonInterpreter();

    /**
     * Initiates all of the scripts.
     */
    public static void initScripts() {
        loadDirectory(Configurations.pythonScriptsDirectory);
        Log.info(PythonScriptManager.class.getCanonicalName(), "Loaded " + pythonCache.size() + " scripts.");
    }

    private static void loadDirectory(String directory) {
        File[] files = new File(directory).listFiles();
        if (files != null) {
            for (File file : files) {
                if (file == null)
                    continue;
                if (file.getName().endsWith(".py")) {
                    pythonCache.put(file.getName(), file.getPath());
                    Log.info(PythonScriptManager.class.getCanonicalName(), "Loaded " + file.getName() + ".");
                } else if (file.isDirectory()) {
                    loadDirectory(file.getAbsolutePath());
                }
            }
        }
    }

    /**
     * Executes a given script.
     *
     * @param script The Script name to find.
     * @return if it executed fine.
     */
    public static boolean executeScript(String script) {
        String file = pythonCache.get(script);
        if (file != null) {
            pythonInterpreter.execfile(file);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Executes a script with the given arguments.
     *
     * @param script The script to execute.
     * @param args   The arguments to pass.
     * @return if we can execute it.
     */
    public static boolean executeScript(String script, Object[]... args) {
        String file = pythonCache.get(script);
        if (file != null) {
            for (Object[] object : args) {
                pythonInterpreter.set((String) object[0], object[1]);
            }
            pythonInterpreter.execfile(Configurations.pythonScriptsDirectory + script);
            return true;
        } else {
            return false;
        }
    }
}