package co.arham.scratch;

/**
 * All of the server's global configurations.
 * <p>
 * Note: these are all loaded via Server's loadConfigurations() method.
 *
 * @author Arham 4
 */
public final class Configurations {

    public static String name;
    public static int port;

    public static String pythonScriptsDirectory;

    private Configurations() {
    }
}
