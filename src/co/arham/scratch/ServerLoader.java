package co.arham.scratch;

import co.arham.scratch.networking.ServerListener;
import co.arham.scratch.networking.login.packets.LoginBlockSize;
import co.arham.scratch.networking.login.packets.LoginConnectionType;
import co.arham.scratch.networking.login.packets.LoginRequest;
import co.arham.scratch.networking.login.packets.LoginResponse;
import co.arham.scratch.utilities.PythonScriptManager;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.minlog.Log;
import com.google.common.base.Stopwatch;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Loads the server.
 *
 * @author Arham 4
 */
class ServerLoader {

    private Server server;

    private ServerLoader() throws IOException {
        Stopwatch stopwatch = Stopwatch.createStarted();
        load();
        stopwatch.stop();

        Log.info(ServerLoader.class.getCanonicalName(), "Server took " + stopwatch.elapsed(TimeUnit.MILLISECONDS)
                + " milliseconds to load");
    }

    public static void main(String[] args) throws IOException {
        Log.set(Log.LEVEL_DEBUG);
        new ServerLoader();
    }

    private void load() throws IOException {
        //load configurations first always
        loadConfigurations();
        loadNetworking();
        loadScripts();
    }

    private void registerPackets() {
        Kryo kryo = server.getKryo();

        kryo.register(LoginRequest.class);
        kryo.register(LoginResponse.class);
        kryo.register(LoginConnectionType.class);
        kryo.register(LoginBlockSize.class);
    }

    private void loadScripts() {
        PythonScriptManager.initScripts();
    }

    private void loadNetworking() throws IOException {
        server = new Server();
        server.bind(Configurations.port);
        server.start();

        server.addListener(new ServerListener());

        registerPackets();
    }

    private void loadConfigurations() throws IOException {
        Properties properties = new Properties();
        String propertiesFileName = "server.properties";
        InputStream inputStream = new FileInputStream(propertiesFileName);
        properties.load(inputStream);

        Configurations.name = properties.getProperty("name");
        Configurations.port = Integer.parseInt(properties.getProperty("port"));

        Configurations.pythonScriptsDirectory = properties.getProperty("pythonScriptsDirectory");

        inputStream.close();
    }
}
