package co.arham.scratch.networking.login.packets;

import co.arham.scratch.networking.login.LoginPacketObserver;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryonet.Connection;
import lombok.Getter;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Handles the login request which is asked when the server connects.
 *
 * @author Arham 4
 */
public class LoginRequest extends LoginPacketObserver implements KryoSerializable {

    private final Random RANDOM = new SecureRandom();
    private Request request;

    @Override
    public void write(Kryo kryo, Output output) {
        output.writeLong(0);
        output.writeByte(0);
        output.writeLong(RANDOM.nextLong());
    }

    @Override
    public void read(Kryo kryo, Input input) {
        byte requestId = input.readByte();
        request = Request.getRequest(requestId);
        input.readByte(); // this is the name hash, but to be honest, its never used, so what's the point.
    }

    @Override
    public void received(Connection connection) {
        validate(connection);
        connection.sendTCP(new LoginRequest());
    }

    private void validate(Connection connection) {
        if (request == null) {
            connection.sendTCP(new LoginResponse(LoginResponse.LoginResponseCode.COULD_NOT_COMPLETE_LOGIN));
            return;
        }
        connection.sendTCP(new LoginResponse(LoginResponse.LoginResponseCode.EXCHANGE_DATA));
    }

    private enum Request {
        GAME_SERVER(14),
        FILE_SERVER(15);

        private static final List<Request> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
        @Getter
        private final int id;

        Request(int id) {
            this.id = id;
        }

        public static Request getRequest(int id) {
            for (Request request : VALUES) {
                if (request.getId() == id) {
                    return request;
                }
            }
            return null;
        }
    }
}
