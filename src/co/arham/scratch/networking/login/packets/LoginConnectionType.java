package co.arham.scratch.networking.login.packets;

import co.arham.scratch.networking.login.LoginPacketObserver;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryonet.Connection;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Sends the type of connection that is about to go through. Normal or reconnection?
 *
 * @author Arham 4
 */
public class LoginConnectionType extends LoginPacketObserver implements KryoSerializable {

    private ConnectionType connectionType;

    @Override
    public void write(Kryo kryo, Output output) {
        // Empty because not writing anything.
    }

    @Override
    public void read(Kryo kryo, Input input) {
        int loginConnectionTypeId = input.readByte();
        connectionType = ConnectionType.getConnectionType(loginConnectionTypeId);
    }

    @Override
    public void received(Connection connection) {
        validate(connection);
    }

    private void validate(Connection connection) {
        if (connectionType == null) {
            connection.sendTCP(new LoginResponse(LoginResponse.LoginResponseCode.LOGIN_SERVER_REJECTED_SESSION));
        }
    }

    private enum ConnectionType {
        NORMAL(16),
        RECONNECTING(18);

        private static final List<ConnectionType> VALUES = Collections.unmodifiableList(Arrays.asList(values()));

        @Getter
        private final int id;

        ConnectionType(int id) {
            this.id = id;
        }

        public static ConnectionType getConnectionType(int id) {
            for (ConnectionType connectionType : VALUES) {
                if (connectionType.getId() == id) {
                    return connectionType;
                }
            }
            return null;
        }
    }
}
