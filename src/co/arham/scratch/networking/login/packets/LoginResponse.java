package co.arham.scratch.networking.login.packets;

import co.arham.scratch.networking.login.LoginPacketObserver;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import lombok.Getter;

/**
 * Represents the login responses sent between the client and server during the login process.
 *
 * @author Arham 4
 */
public class LoginResponse extends LoginPacketObserver implements KryoSerializable {

    /**
     * Unfortunately can't use LoginResponseCode only because then it can't be easily sent through the Output class.
     */
    private final int opcode;

    public LoginResponse(LoginResponseCode loginResponseCode) {
        this.opcode = loginResponseCode.getOpcode();
    }

    @Override
    public void write(Kryo kryo, Output output) {
        output.writeByte(opcode);
    }

    @Override
    public void read(Kryo kryo, Input input) {
        // Empty because not reading anything.
    }

    /**
     * Represents the enumerated login response.
     *
     * @author SeVen
     */
    public enum LoginResponseCode {
        EXCHANGE_DATA(0),
        DELAY(1),
        NORMAL(2),
        INVALID_CREDENTIALS(3),
        ACCOUNT_DISABLED(4),
        ACCOUNT_ONLINE(5),
        GAME_UPDATED(6),
        WORLD_FULL(7),
        LOGIN_SERVER_OFFLINE(8),
        LOGIN_LIMIT_EXCEEDED(9),
        BAD_SESSION_ID(10),
        LOGIN_SERVER_REJECTED_SESSION(11),
        MEMBERS_ACCOUNT_REQUIRED(12),
        COULD_NOT_COMPLETE_LOGIN(13),
        SERVER_BEING_UPDATED(14),
        RECONNECTING(15),
        LOGIN_ATTEMPTS_EXCEEDED(16),
        MEMBERS_ONLY_AREA(17),
        INVALID_LOGIN_SERVER(20),
        TRANSFERRING_PROFILE(21);

        @Getter
        private final int opcode;

        LoginResponseCode(int opcode) {
            this.opcode = opcode;
        }
    }
}
