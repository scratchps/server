package co.arham.scratch.networking.login.packets;

import co.arham.scratch.networking.login.LoginPacketObserver;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * Receives the login block size before sending the login block to make sure we (the client and the server) are on the
 * same level.
 *
 * @author Arham 4
 */
public class LoginBlockSize extends LoginPacketObserver implements KryoSerializable {

    private int size;

    @Override
    public void write(Kryo kryo, Output output) {
        // Empty because not writing anything.
    }// TODO remove size and integrate with login block

    @Override
    public void read(Kryo kryo, Input input) {
        size = input.readInt();
    }

    public int getSize() {
        return size;
    }
}
