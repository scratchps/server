package co.arham.scratch.networking;

import co.arham.scratch.networking.login.LoginPacketObserver;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

/**
 * Listens to the incoming requests/demands.
 *
 * @author Arham 4
 */
public class ServerListener extends Listener {

    @Override
    public void received(Connection connection, Object object) {
        LoginPacketObserver loginPacketObserver = (LoginPacketObserver) object;
        if (loginPacketObserver != null) {
            loginPacketObserver.received(connection);
        }
    }
}
